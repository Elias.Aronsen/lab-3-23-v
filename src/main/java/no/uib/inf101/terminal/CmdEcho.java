package no.uib.inf101.terminal;

public class CmdEcho implements Command{

    @Override
    public String run(String[] args) {
        String words = "";
        for( String word : args){
            words += (word + " ");
        }
        
        return words;
    }

    @Override
    public String getName() {
        
        return "echo";
    }

    @Override
    public String getManual() {
        return "Allows you to have the terminal echo your words";
    }
    
}

package no.uib.inf101.terminal;

import java.util.Map;

public interface Command {



    String run(String[] args);



    String getName();


    default void setContext(Context context) { /* do nothing */ };
    


    
    default void setCommandContext(Map<String, Command> commandContext) { /* do nothing */ }

    String getManual();


}

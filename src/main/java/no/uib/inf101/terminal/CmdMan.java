package no.uib.inf101.terminal;

import java.util.Map;

public class CmdMan implements Command {
    
    private Map<String, Command> cContext;

    @Override
    public String run(String[] args) {
        if(cContext.containsKey(args[0])){
            return cContext.get(args[0]).getManual();
        } else {
            return "man: no such command: " + args[0];
          }
    }

    @Override
    public String getName() {
        return "man";
    }

    @Override
    public String getManual() {
        return "Manual for the command";
    }

    @Override
    public void setCommandContext(Map<String, Command> commandContext) {
        this.cContext = commandContext;
    }
    
}
